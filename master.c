#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/epoll.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <sys/resource.h>

//Enable this for debugging
//#define DEBUG 1

//Enable for restarting failed process
//#define RESTART 1

#define READ_END 0
#define WRITE_END 1
#define EPOLL_TIMEOUT 1000
#define BUFF_SIZE 512
#define MAX_PATH_LEN 100
#define MAX_MECHANISM_LEN 100
#define STD_FDCOUNT 3

#ifdef RESTART
#define MAX_RESTARTS 10
#endif

//static variable for arg parsing
static int xValue, nValue = -1, max_workers;
static char mechanism[MAX_PATH_LEN], path[MAX_MECHANISM_LEN];
static bool xFlag = false, nFlag = false, mFlag = false, pFlag = false,\
                    wFlag = false;

//Limits
static int MAX_FDS;

//Hashmap of Worker id and corresponding FD.
static int *efd_arr;
static int epfd;
static int finished_workers, id, rfd_count;

//sfd_arr act as hashmap of worker ID to corresponding FD
static int *sfd_arr;
static fd_set readfds;

//Used for restarting the failed process
#ifdef RESTART
static int *pid_arr;
static int restart_count;
#endif

/*
 * Execs a Child process 
 * Args:
 *  fd - 2 FDs of PIPE's output
 *  index - Worker's ID
 * Returns:
 *  None
 * */
void exec_child_process(int fd[], int index){
    //Child Process
    //Close the READ end of the PIPE. 
    close(fd[READ_END]);
    //Duplicate STDOUT
    if(dup2(fd[WRITE_END], STDOUT_FILENO) == -1){
        perror("Dup2");
        close(fd[WRITE_END]);
        exit(EXIT_FAILURE);		
    }
    //Replace with Worker Process
    char xStr[10], nStr[10];
    sprintf(xStr, "%d", xValue);
    sprintf(nStr, "%d", index);
    if(execl(path, path, "-x", xStr, "-n", nStr,  NULL) == -1){
        perror("Execl");
        close(fd[WRITE_END]);
        exit(EXIT_FAILURE);
    }
}

/*
 * Forks a new child process for select computation
 * Args:
 *  w_id      - Worker ID
 * Returns:
 *  None
 * */
void fork_child_select(int w_id){
    int fd[2];
    if(pipe(fd) < 0){
        perror("Pipe in select");
        exit(EXIT_FAILURE);	
    }
    pid_t pid = fork();
    if(pid < (pid_t)0){
        perror("fork in select failed");
        exit(EXIT_FAILURE);	
    }
    else if(pid == (pid_t)0){
        //call Child Process
        exec_child_process(fd, w_id);
    }
    else{
        //Parent Process
        //Close the WRITE end of the PIPE.
        close(fd[WRITE_END]);
        FD_SET(fd[READ_END], &readfds);
        sfd_arr[w_id] = fd[READ_END];
#ifdef DEBUG
        printf("Forked a child process %d\n", w_id);
#endif
#ifdef RESTART	
        pid_arr[w_id] = pid;
#endif
    }
}

/*
 * Forks a child process for Epoll computation
 * Args:
 *  w_id    - Worker ID
 * Returns:
 *  None
 * */
void fork_child_epoll(int w_id){
    int fd[2];
    if(pipe(fd) < 0){
        perror("Pipe error:");
        exit(EXIT_FAILURE);	
    }	
    pid_t pid = fork();
    if(pid < (pid_t)0){
        perror("Fork in Epoll");
        exit(EXIT_FAILURE);
    }
    else if(pid == (pid_t)0){
        //call Child Process
        exec_child_process(fd, w_id);
    }
    else{
        //Parent Process
        //Close the WRITE end of the PIPE.
        close(fd[WRITE_END]);
        struct epoll_event event;
        event.data.fd = fd[READ_END];
        event.events = EPOLLIN;
        int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, fd[READ_END], &event);
        if(ret){
            perror("Error in Epoll_ctl:");
            exit(EXIT_FAILURE);			
        }
#ifdef DEBUG
        printf("Forked a child process %d\n", w_id);
#endif
#ifdef RESTART	
        pid_arr[w_id] = pid;
#endif

        efd_arr[fd[READ_END]] = w_id;
    }

}
#ifdef RESTART
/*
 * Restarts worker process based on the ID
 * Args:
 *  w_id - Worker ID
 * Returns:
 *  None
 * */
void restart_worker(int w_id){
#ifdef DEBUG
    printf("Restarting WID:%d\n", w_id);
#endif
    if(restart_count >= MAX_RESTARTS){
        printf("REACHED max Restarts... Check worker...Exiting\n");
        exit(EXIT_FAILURE);
    }
    if(w_id < 0){
#ifdef DEBUG
        printf("Unknown WID\n");
#endif
        return;
    }
    if(strcmp(mechanism, "epoll") == 0){
        if(id < nValue){
            fork_child_epoll(w_id);	
            rfd_count++;
            restart_count++;
        }
    }
    else if(strcmp(mechanism, "select") == 0){
        if(id < nValue){
            fork_child_select(w_id);
            rfd_count++; 
            restart_count++;
        }
    }
    else{
        return;	
    }
}

/*
 * Get WorkerID from ProcessID
 * Args:
 *  pid - ProcessID
 * Returns:
 *  w_id - WorkerID
 * Iterate through pid_arr and find index
 */
int get_wid_from_pid(int pid){
    int k;	
    for(k = 0; k < nValue; k++){
        if(pid_arr[k] == pid)
            return k;	
    }
#ifdef DEBUG
    printf("Unknown PID\n");
#endif
    return -1;
}
#endif

/*
 * Forks a new worker process based on the mechansim
 * Args:
 *  None
 * Returns:
 *  None
 * */
void fork_new_worker(){
    if(strcmp(mechanism, "epoll") == 0){
        if(id < nValue){
            fork_child_epoll(id);	
            rfd_count++; id++;
        }
    }
    else if(strcmp(mechanism, "select") == 0){
        if(id < nValue){
            fork_child_select(id);
            rfd_count++; id++;	
        }
    }
    else{
        return;	
    }
}

/*
 * Handles SIGCHLD and checks the exit status  of child process.
 * If the status is not 0, the process exits.
 * */
void sigchld_handler(){
    int wstat;
    pid_t pid;
    while(1){
        pid = waitpid(-1, &wstat, WNOHANG);
        if(pid == 0 || pid == -1)
            return;
        else{
            if(wstat != 0){
                printf("Fatal PID: %d failed with error code %d\n", \
                        pid, wstat);	
#ifdef RESTART		
                int w_id = get_wid_from_pid(pid);
                restart_worker(w_id);
                continue;
#endif		
                exit(EXIT_FAILURE);
            }
            else{
                //One Worker has successfully completed.
#ifdef DEBUG
                printf("Worker(%d) has successfully completed\n", pid);
#endif
                fork_new_worker();
            }
        }
    }

}

/*
 * Gets the result of a worker process 
 * Args:
 *  fd - Master process' READ end of PIPE FD
 * Returns:
 *  Result - The return value of worker converted to double. 
 * */
double get_result_from_worker(int fd){
    //READ from Worker Process.
    char buff[BUFF_SIZE];
    ssize_t len;
    if((len = read(fd, buff,BUFF_SIZE-1)) < 0){
        perror("Read");
        exit(EXIT_FAILURE);				
    }
    buff[len] = '\0';
    printf("%s\n", buff);
    return atof(buff);
}

/*
 * Sequential computation of exponential series
 * Args:
 *  None
 * Returns:
 *  None
 * Max_workers has no effect here.
 * */
void sequential_computation(){
    double result = 0;
    for(id = 0; id < nValue; id++){		    
        int fd[2];
        pipe(fd);
        pid_t pid = fork();
        if(pid < (pid_t)0){
            perror("Fork");
            exit(EXIT_FAILURE);		
        }
        else if(pid == (pid_t)0){
            //Call Child Process
            exec_child_process(fd, id);
        }
        else{
            //Parent Process
            int wstat;
            if(pid == waitpid(pid, &wstat, 0)){
                if(wstat != 0){
                    printf("Fatal PID:%d failed with status %d\n", pid, wstat);
#ifdef RESTART
                    close(fd[WRITE_END]);
                    close(fd[READ_END]);
                    id--;
                    if(restart_count >= MAX_RESTARTS){
                        printf("Did max Restarts... Check worker...Exiting\n");
                        exit(EXIT_FAILURE);
                    }
                    restart_count++;
                    continue;
#endif				
                    exit(EXIT_FAILURE);
                }
                //Close the WRITE end of the PIPE.
                close(fd[WRITE_END]);
                printf("Worker %d: %d^%d/%d! : ", id, xValue, id, id);
                //READ from Worker Process.
                result += get_result_from_worker(fd[READ_END]);
                close(fd[READ_END]);    
            }
        }
    }
    printf("Final: %f\n", result);
}

/*
 * Check if we need to wait for any worker process to complete
 * Args:
 *  rfd_count - Number of open FDs from worker process
 *  finished_workers - Number of finished Workers
 *  result - Final result
 * Returns:
 *  None
 * */
int is_wait_needed(int rfd_count, int finished_workers, double result){
    if(rfd_count == 0 && finished_workers == nValue){
        printf("Final: %f\n",result);
        exit(EXIT_SUCCESS);		
    }
    if(rfd_count == 0 ){
        //wait for atleast one of child to finish/one worker to be running
#ifdef DEBUG
        printf("Waiting for child to finish\n");
#endif
        sleep(1);		
        return 1;
    }
    return 0;
}

/*
 * Checks the RV of Poll/Epoll/Select and prints the result
 * It may be an actual error or timeout which may mean completion of 
 * the computation.
 * Args:
 *  finished_workers  - #of finished workers
 *  result            - Final Result
 * Returns:
 *  If the work is actually completed, it prints the result
 *  Else prints error message.
 *  Incase of EINTR, returns the errno. Else returns 0
 * */
int check_and_print_result(int finished_workers, double result){
    if(finished_workers == nValue){
        printf("Final: %f\n", result);  
        exit(EXIT_SUCCESS);
    }
    else{
        if(errno == EINTR || errno == 0)
            return errno;
        perror("Poll/Epoll/Select failure:");
        exit(EXIT_FAILURE);	
    }	
}

/*
 * Initialize sfd_arr
 * */
void init_sfd_arr(){
    sfd_arr = (int *)malloc(sizeof(int) * nValue);
    if(!sfd_arr){
        perror("malloc sfdarr");
        exit(EXIT_FAILURE);	
    }
    memset(sfd_arr, -100, nValue*sizeof(int));
}

/*
 * Computes the function using SELECT
 * Args:
 *  None(Uses statically defined variables)
 * Returns:
 *  None(Prints the result/failure)
 * */
void select_computation(){	
    int k;
    //For holding the running sum.
    double result = 0;
    init_sfd_arr();
    //Read FDS
    fd_set curr_readfds;
    FD_ZERO(&readfds);
    FD_ZERO(&curr_readfds);
    struct timeval tv;
    //Fork max_workers amount of worker processes
    for(id = 0; id < max_workers; id++, rfd_count++)
        fork_child_select(id);            

    while(1){		        
        curr_readfds = readfds;
        tv.tv_sec = 1; tv.tv_usec = 0;
        if(is_wait_needed(rfd_count, finished_workers, result)) 
            continue;

        int rv = select(MAX_FDS, &curr_readfds, NULL, NULL, &tv);
        //This may be completion or error
        if(!rv){
            int ret = check_and_print_result(finished_workers, result);
            if(ret == EINTR || ret == 0)
                continue;			
        }
        for(k = 0; k < nValue && rv > 0; k++){
            if(FD_ISSET(sfd_arr[k], &curr_readfds)){
                printf("Worker %d: %d^%d/%d! : ", k, xValue, k, k);
                result += get_result_from_worker(sfd_arr[k]);
                FD_CLR(sfd_arr[k], &readfds);
                close(sfd_arr[k]);	
                sfd_arr[k] = -100;
                finished_workers++; rv--; rfd_count--;					                
            }
        }
    }
}

/*
 * Initialize efd_arr
 * */
void init_efd_arr(){
    efd_arr = (int *)malloc(sizeof(int) * MAX_FDS);
    if(!efd_arr){
        perror("malloc sfdarr");
        exit(EXIT_FAILURE);	
    }
    memset(efd_arr, -1, nValue*sizeof(int));
}

/*
 * Compute the function using EPOLL
 * Args:
 *  None (all variables are static)
 * Returns:
 *  None (just prints the Final value/errors)
 * */
void epoll_computation(){
    epfd = epoll_create(nValue);
    //For holding the running sum.
    double result = 0;
    int j;
    struct epoll_event *events;
    events = malloc(sizeof(struct epoll_event) * max_workers);
    if(!events){
        printf("Out of memory\n");
        exit(EXIT_FAILURE);	
    }
    init_efd_arr();
    //create max_workers processes
    for(id = 0; id < max_workers; id++, rfd_count++)
        fork_child_epoll(id);		

    //Whenever we sucessfully read output from a worker, 
    //we create another worker process till we have created N workers
    while(1){
        if(is_wait_needed(rfd_count, finished_workers, result)) 
            continue;
        int rv = epoll_wait(epfd, events, max_workers, EPOLL_TIMEOUT);
        //This may be an error or completion. 
        if(!rv){
            int ret = check_and_print_result(finished_workers, result);
            if(ret == EINTR || ret == 0)
                continue;			
        }

        for(j = 0; j < rv; j++){
            int fd = events[j].data.fd;
            int w_id = efd_arr[fd];			
            printf("Worker %d: %d^%d/%d! : ", w_id, xValue, w_id, w_id);			
            result += get_result_from_worker(fd);
            //Remove the FD from epfd.
            int ret = epoll_ctl(epfd, EPOLL_CTL_DEL, fd, NULL);
            if(ret){
                perror("Error in Epoll_ctl_del");
                exit(EXIT_FAILURE);			
            }
            close(fd);
            finished_workers++; rfd_count--;			    
        }		
    }	
    free(events);
}

/*
 * Prints usage of the program.
 * */
void usage(){
    printf("USAGE: ./master --worker_path ./worker --num_workers 5 \
            --wait_mechanism MECHANISM -x 2 -n 12\n");
    exit(EXIT_FAILURE);	
}

#ifdef RESTART
/*
 * Initialize PID array that maps PID to Worker ID */
void init_pid_array(){
    pid_arr = (int *)malloc(sizeof(int) * nValue);
    if(!pid_arr){
        perror("malloc pid_arr");
        exit(EXIT_FAILURE);	
    }
    memset(pid_arr, 0, sizeof(int)*nValue);
}
#endif

/*
 * Sets FD and Worker pool counts according to RLIM_NOFILE
 * Args:
 * None
 * Returns:
 * None
 * Get the RLIMIT and set the MAX_FDS, max_workers accordingly.
 * */
void set_fd_worker_limits(){
    struct rlimit rlim;
    if(getrlimit(RLIMIT_NOFILE, &rlim) < 0){
        perror("GETRLIM");	
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG
    printf("Limits: CUR %ld \t MAX %ld\n", rlim.rlim_cur, rlim.rlim_max);
#endif
    int max_pool_size = (rlim.rlim_cur / 2) - STD_FDCOUNT;
    MAX_FDS = rlim.rlim_cur;

    if(max_workers > max_pool_size){
#ifdef DEBUG
        printf("Reduce pool size to %d due to limitations\n", max_pool_size);
#endif
        max_workers = max_pool_size;
    }
}
/*
 * Parse the command line arguments
 * Args:
 *  argc - Arguments count
 *  argv - Array of char arrays
 * Returns:
 *  None
 *  Just sets the static variables accordingly.
 * */
void parse_args(int argc, char **argv){
    int c;
    strcpy(path, "");	
    while(1)	{
        static struct option long_options[] = {
            {"wait_mechanism", required_argument, 0, 'm'},
            {"worker_path", required_argument, 0, 'p'},
            {"num_workers", required_argument, 0, 'w'}
        };
        int index = 0;
        c = getopt_long (argc, argv, "x:n:m:p:", long_options, &index);
        if(c == -1){
            break;
        }
        switch(c){
            case 'x':
                xValue = atoi(optarg);
                xFlag = true;
                break;
            case 'n':
                nValue = atoi(optarg);
                nFlag = true;
                break;
            case 'm':
                strcpy(mechanism, optarg);
                mFlag = true;
                break;
            case 'p':
                strcpy(path, optarg);
                pFlag = true;
                break;	
            case 'w':
                max_workers = atoi(optarg);
                wFlag = true;
                break;
            default:
                break;
        }  
    }
}

/*
 * Validate Arguments
 * Args:
 *  None(All the values are statically defined)
 * Returns:
 *  None if the args are valid, Exits the process if anything is invalid
 * */
void validate_args(){
    if(!pFlag || (strcmp(path, "") == 0)){
        printf("Worker_path arg is not present/invalid\n");
        usage();
    }
    if(!wFlag || (max_workers <= 0)){
        printf("num_workers is mandatory and can't be <= 0...exiting\n");
        usage();
    }
    if(!xFlag || xValue <= 0)
    {
        printf("x is mandatory and should be positive...exiting\n");
        usage();
    }
    if(!nFlag || nValue < 0){
        printf("n is mandatory and should be positive...exiting\n");
        usage();	
    }
    if(!mFlag){
        printf("Mechanism must be mentioned\n");
        usage();	
    }
}

int main(int argc, char **argv){	

    //Parse the arguments
    parse_args(argc, argv);

    //Validate the arguments
    validate_args();

    //Set max workers to nValue to avoid comparisons later on.
    if(max_workers >= nValue)
        max_workers = nValue;	

    set_fd_worker_limits();

#ifdef RESTART
    init_pid_array();
#endif

    //To check exit status of child process
    signal(SIGCHLD, sigchld_handler);

    //Based on the mechanism call appropriate functions
    if(strcmp(mechanism, "sequential") == 0)
        sequential_computation();
    else if(strcmp(mechanism, "select") == 0)
        select_computation();
    else if(strcmp(mechanism, "epoll")  == 0)	
        epoll_computation();
    else{
        printf("Unknown Mechanism...exiting\n");
        usage();
    }
    return 0;
}
