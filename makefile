all: master worker

clean: 
	rm -rf master worker core

master:
	gcc -g -Wall -Werror -fpic master.c -o master

worker:
	gcc -g -Wall -Werror -fpic worker.c -lm -o worker

select: master worker
	./master  --num_workers 2 --worker_path ./worker --wait_mechanism select -x 2 -n 12

sequential: master worker
	./master --worker_path ./worker --num_workers 10 --wait_mechanism sequential -x 2 -n 14

epoll: master worker
	./master --worker_path ./worker --num_workers 4 --wait_mechanism epoll -x 2 -n 13

test: worker
	./worker -x 2 -n 3

select1: master worker
	./master  --num_workers 12 --worker_path ./worker --wait_mechanism select -x 2

selectL: master worker
	./master  --num_workers 1200 --worker_path ./worker --wait_mechanism select -x 2 -n 11500

epollL: master worker
	./master  --num_workers 1200 --worker_path ./worker --wait_mechanism epoll -x 2 -n 11500
