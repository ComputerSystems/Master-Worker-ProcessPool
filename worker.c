/*
 * Sriramprabhu Sankaraguru
 * 001648794
 * */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>

//enable this for debugging
#define DEBUG 1

//enable to test restart
//#define RESTART 1

/*
 * Prints usage of the program.
 * */
void usage(){
    printf("Usage: ./worker -x 2 -n 12\n");
    exit(EXIT_FAILURE);
}

/*
 * Computes n factorial
 * Args:
 *  n - a number
 * Returns:
 *  Fact(n);
 * */
double fact(int n){
    double nFact = 1;
	int i;
    for(i = 2; i <= n; i++){
        nFact = nFact * i;
    }
    return nFact;
}

/*
 * Parses arg list and loads xValue and nValue
 * Args:
 *  argc    - Argument count
 *  argv    - Array of char array (Argument list)
 *  xValue  - Pointer to xValue that holds X
 *  nValue  - Pointer to nValue that holds N
 * Returns:
 *  None
 * */
void parse_args(int argc, char **argv, int *xValue, int *nValue){
	int c;
    bool xFlag = false, nFlag = false;
    while((c = getopt(argc, argv, "x:n:")) != -1){
        switch(c){
            case 'x':
                *xValue = strtol(optarg, NULL, 10);
                xFlag = true;
                break;
            case 'n':
                *nValue = strtol(optarg, NULL, 10);
                nFlag = true;
                break;
            default:  
                usage();
        }
    }
    if(!xFlag){
		printf("X value is mandatory\n");
        usage();
    } 
    if(!nFlag){
		printf("N value is mandatory\n");
        usage();
    } 
    if(*xValue < 0 || *nValue < 0){
        printf("X and N values should be positive\n");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char** argv){
    int xValue, nValue;
	//Parse and load xValue, nValue
	parse_args(argc, argv, &xValue, &nValue);
    double xn = pow(xValue, nValue);
    double nFact = fact(nValue);  
    double result = xn/nFact;
    struct stat sb;
    if(fstat(STDOUT_FILENO, &sb) < 0){
        perror("fstat");	
        exit(EXIT_FAILURE);
    }
	//Check if its PIPE FD
    if((sb.st_mode & S_IFMT) == S_IFIFO){
        //Assuming DUP...writing to write-end of the pipe
		printf("%f", result);
    }
    else{
        //Print to STDOUT with help Text.
        printf("x^n / n! : %f\n", result);     
    }
	#ifdef DEBUG
	//We can modify the timing for testing here.
	if((nValue % 2) == 0){
		sleep(1);
	}
	else
		sleep(1);
	#endif
	#ifdef RESTART
	if((nValue % 2) == 0)
		exit(EXIT_FAILURE);
	#endif
    exit(EXIT_SUCCESS);
}
