# Master-Worker Process Model

### Problem Description

Our aim is to compute exponential function e^x = 1 + x + x^2 / 2! + x^3 / 3+
.... for n in [0..n-1]. 

Our master will distribute each component to a worker process and aggregates the
result from the 
workers to return the cumulative result. 

1. Worker is capable of finding x^n/n! for one n. i.e. if x= 2, n = 3, worker
   will return 2^3/3!. 
2. If the process is called from master, it will print just the computed value.
   i.e. 1.333 But if the worker was called in standalone mode, it should return
x^n/n! : 1.333
3. We need to do this using select, epoll system calls and in sequential order
4. There is a limit on Maximum workers can be running at any time. We should not
   create more processes than max_workers
5. Whenever a process exits, we should start the next process.
6. In case of failure, we should restart the exact worker process. (We restart
   upto MAX_RESTART times. After that, we exit)

### Design
1. We solve this using Pipe(). 
2. Master opens FD and dups with STDOUT. 
3. Master forks upto max_workers amount of worker process and sends appropriate
   x and n value.
4. Worker computes the result and just prints it to STDOUT.
5. Master can read it back.
6. Master registers for SIGCHLD signal. 
7. Inside that handler, we start the next worker process if the child exits
   successfully.
8. Otherwise, we restart the failed process based on the PID.
9. x and n values, PIDs and corresponding worker index are maintained for
   restart and starting next worker process.

### Setup
Place Master.c and worker.c files in the CWD.

Enable DEBUG flag for debugging
Enable RESTART flag to restart the failed worker processes

Use 
1. make clean      - To clean the binaries
2. make test       - To test the worker alone with x = 2 and n = 3
3. make sequential - Performs sequential computation 
4. make select     - Computation using select()
5. make epoll      - Computation using epoll()

### Challenges/Learnings
1. Very good understanding of select, pipe, fork, epoll. 
2. Maintaining the variables s.t restarting, starting next process and
   monitoring the results are easier.


